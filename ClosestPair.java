package ClosestPair;

public class ClosestPair {

    public final static double INF = java.lang.Double.POSITIVE_INFINITY;
    /** 
     * Given a collection of points, find the closest pair of point and the
     * distance between them in the form "(x1, y1) (x2, y2) distance"
     *
     * @param pointsByX points sorted in nondecreasing order by X coordinate
     * @param pointsByY points sorted in nondecreasing order by Y coordinate
     * @return Result object containing the points and distance
     */
    static Result findClosestPair(XYPoint pointsByX[], XYPoint pointsByY[]) {
    	
    	double default_min_dist = INF;
    	double current_min_dist = default_min_dist;
    	int num_of_pts = pointsByX.length;
    	
    	/*
    	 * Handle the base cases where num_of_pts<3
    	 */
    	if(num_of_pts<3){
	        if (num_of_pts == 0){
	        	XYPoint pt = new XYPoint(0,0);
	        	return new Result(pt,pt,INF);
	        }else if(num_of_pts==1){
	        	return new Result(pointsByX[0],pointsByX[0], INF);
	        }else if (num_of_pts == 2){
	            return new Result(pointsByX[0], pointsByX[1], pointsByX[0].dist(pointsByX[1]));
	        }
    	}
        
    	/*
    	 * Split up the points into two halves (left and right side portions)
    	 * also hold the pivot point for use later
    	 */
        int mid_pt_indx = num_of_pts / 2;
        XYPoint midPoint = pointsByX[mid_pt_indx];
        
        /*
         * create the arrays to hold all the partitioned points
         */
        XYPoint left_pts_by_X[] = new XYPoint [mid_pt_indx];
        XYPoint right_pts_by_X[] = new XYPoint [num_of_pts - mid_pt_indx];
        XYPoint left_pts_by_Y[] = new XYPoint [mid_pt_indx];
        XYPoint right_pts_by_Y[] = new XYPoint [num_of_pts - mid_pt_indx];
         
        /*
         * populate the different arrays defined by x sorted points
         */
        int i;
        for (i = 0; i < mid_pt_indx; i++)
            left_pts_by_X[i] = pointsByX[i];
        for (i = mid_pt_indx; i < num_of_pts; i++)
            right_pts_by_X[i - mid_pt_indx] = pointsByX[i];
         
        //counter for the number of points from the left side sorted by Y
        int counter_ptsByY_L = 0;
        //counter for the number of points from the right side sorted by Y
        int counter_ptsByY_R = 0;
        
        /*
         * populate the different arrays defined by y sorted points
         * ie: the points on the left side sorted by their y coordinates
         * and the points on the right side sorted by their y coordinates
         */
        for (i = 0; i < num_of_pts; i++){
            if (pointsByY[i].isLeftOf(midPoint)){
                left_pts_by_Y[counter_ptsByY_L] = pointsByY[i];
                counter_ptsByY_L++;
            }else{
                right_pts_by_Y[counter_ptsByY_R] = pointsByY[i];
                counter_ptsByY_R++;
            }
        }
        
        /*
         * recursively call the algorithm on the left side points and right side points
         */
        Result curr_Left_Side_CP  = findClosestPair(left_pts_by_X, left_pts_by_Y);
        Result curr_Right_Side_CP = findClosestPair(right_pts_by_X, right_pts_by_Y);
         
        /*
         * Properly orient the points in both results.
         */
        check_CP(curr_Left_Side_CP);
        check_CP(curr_Right_Side_CP);
        
        /*
         * default the final result to the return from the the left side call, 
         * because if things are equal we return the left most
         */
        Result final_CP = curr_Left_Side_CP;  //default
        /*
         * Check to see if the right side found a closer pair than the left side
         * if so replace the default left side CP with the one from the right side
         * otherwise the left side is defaulted as the closest even if the two are equal. 
         */
        if (curr_Left_Side_CP.dist > curr_Right_Side_CP.dist)
            final_CP  = curr_Right_Side_CP;
        else if(curr_Left_Side_CP.dist == curr_Right_Side_CP.dist)
        	compare_CPs(curr_Right_Side_CP, final_CP);
        //else do nothing, leave as default left
        
        /*
         * Set the current minimum distance to be compared to as the minimum between 
         */
        current_min_dist = final_CP.dist;
         
        /*
         * create a vertical strip of points in a band with width 2*current_min_dist 
         * that straddles the midpt
         */
        int y_strip_max_size_possible = num_of_pts;
        XYPoint y_strip[] = new XYPoint [y_strip_max_size_possible];
        int strip_pts_counter = 0;
        for (i = 0; i < y_strip_max_size_possible; i++){
            if (abs(pointsByY[i].x - midPoint.x) < current_min_dist){
                y_strip[strip_pts_counter] = pointsByY[i];
                strip_pts_counter++;
            }
        }
        
        /*
         * Create a rectangular box of dimensions current_min_dist x 2*current_min_dist 
         * to move along a vertical strip of points. This is accomplished functionally by 
		 * simply checking the height compared to a current points and constraining the 
		 * comparison points to the strip of appropriate width that we defined before that 
		 * have a y coordinate less than the current_min_dist away from the point in question 
         */
        int num_pts_in_y_strip = strip_pts_counter;
        int j;
        for (i = 0; i < num_pts_in_y_strip; i++){
            XYPoint strip_pt_in_question = y_strip[i];
             
            for (j = i+1; j < num_pts_in_y_strip; j++){
                XYPoint strip_pt_to_be_compared = y_strip[j];
                 
                if (strip_pt_to_be_compared.y - strip_pt_in_question.y < current_min_dist){
	                double dist = strip_pt_in_question.dist(strip_pt_to_be_compared);
	                if (dist < current_min_dist){
	                    current_min_dist = dist;
	                    final_CP = new Result(strip_pt_in_question, strip_pt_to_be_compared, current_min_dist);
	                    check_CP(final_CP);
	                }
                }else if(strip_pt_to_be_compared.y - strip_pt_in_question.y == current_min_dist){
                	Result temp_CP = new Result(strip_pt_in_question, strip_pt_to_be_compared, current_min_dist);
                	check_CP(temp_CP);
                	compare_CPs(temp_CP,final_CP);
                }else{
                	/*
                	 * Do nothing, because we don't care about these points, they are outside the rectangle.
                	 */
                }
            }
        }
        
        check_CP(final_CP);
        return final_CP;
    }
    
  /**
   * Checks to see if the points in the result are oriented properly... if not it switches them
   * @param final_CP
   */
    static void check_CP(Result final_CP){
    	if(final_CP.p2.isLeftOf(final_CP.p1)){     		//	p2.x < p1.x	or 	(p2.x = p1.x and p2.indx < p1.indx)
        	if(final_CP.p1.x == final_CP.p2.x){			//	p2.x = p1.x
        		if(final_CP.p2.isBelow(final_CP.p1)){ 	// 	p2.x = p1.x and (p2.y < p1.y or p2.y = p1.y and p2.indx < p1.indx)
            		XYPoint temp = final_CP.p1;
                	final_CP.p1 = final_CP.p2;
                	final_CP.p2 = temp;
            	}
        												//	p2.x = p1.x and p1.y < p2.y
        	}else{										
        		XYPoint temp = final_CP.p1;
        		final_CP.p1 = final_CP.p2;
        		final_CP.p2 = temp;
        	}
        }else if(final_CP.p1.x == final_CP.p2.x){ 		//	p2.x = p1.x and p1.index < p2.indx
        	if(final_CP.p2.isBelow(final_CP.p1)){ 		//	p2.x = p1.x and p1.index < p2.indx,  and p2.y<p1.y or p1.y=p2.y but p2.indx<p1.indx
        		XYPoint temp = final_CP.p1;
            	final_CP.p1 = final_CP.p2;
            	final_CP.p2 = temp;
        	}
        }
    }
    
    /**
     * Compare two Closest Pairs and replace the final with the temp if it should be replaced
     * @param temp_CP (Closest Pair Result that might be the right answer)
     * @param final_CP (The current answer that is in question)
     */
    static void compare_CPs(Result temp_CP, Result final_CP){
    	if(temp_CP.dist<final_CP.dist){
    		final_CP.p1 = temp_CP.p1;
    		final_CP.p2 = temp_CP.p2;
    		final_CP.dist = temp_CP.dist;
    	}else if(temp_CP.dist == final_CP.dist){
    		if(temp_CP.p1.x<final_CP.p1.x){
    			//swap because temp is to the left of final
    			final_CP.p1 = temp_CP.p1;
    			final_CP.p2 = temp_CP.p2;
    			//final_CP.dist = temp_CP.dist;
    		}else if(temp_CP.p1.x == final_CP.p1.x){
    			if(temp_CP.p1.y<=final_CP.p1.y){
    				if(temp_CP.p1.y == final_CP.p1.y){
    					if(temp_CP.p1.num < final_CP.p1.num){
    						//swap because the 1st pts of temp and final are equal but temp's p1 is lower index than finals p1
    						final_CP.p1 = temp_CP.p1;
    		    			final_CP.p2 = temp_CP.p2;
    		    			final_CP.dist = temp_CP.dist;
    					}else if(temp_CP.p1.num == final_CP.p1.num){
    						compare_CPs_2(temp_CP,final_CP);
    					}
    					
    				}else{//swap because temp is below final and their x's are equal
	    				final_CP.p1 = temp_CP.p1;
	        			final_CP.p2 = temp_CP.p2;
	        			final_CP.dist = temp_CP.dist;
    				}
    			}
    			//do nothing because the final 
    		}
    	}else{
    		//do nothing... the final has a smaller distance.  
    	}
    }
    
    /**
     * Compare two Closest Pairs and replace the final with the temp if it should be replaced
     * @param temp_CP (Closest Pair Result that might be the right answer)
     * @param final_CP (The current answer that is in question)
     */
    static void compare_CPs_2(Result temp_CP, Result final_CP){
    	if(temp_CP.p2.x < final_CP.p2.x){
    			//swap because temp is to the left of final
    			final_CP.p1 = temp_CP.p1;
    			final_CP.p2 = temp_CP.p2;
    	}else if(temp_CP.p2.x == final_CP.p2.x){
    		if(temp_CP.p2.y <= final_CP.p2.y){
    			if(temp_CP.p2.y == final_CP.p2.y){
    				if(temp_CP.p2.num < final_CP.p2.num){
    						//swap because the 2nd pts of temp and final are equal 
    						//but temp's p2 was created before finals p2
    						final_CP.p1 = temp_CP.p1;
    		    			final_CP.p2 = temp_CP.p2;
    				}else if(temp_CP.p2.num == final_CP.p2.num){
    						//these are the same CP so it doesn't matter
    				}
    			}else{
    				//swap because temp is below final and their x's are equal
    				final_CP.p1 = temp_CP.p1;
        			final_CP.p2 = temp_CP.p2;
        		}
    		}
    			//do nothing because the final 
    	}
    	else{
    		//do nothing... the final has a smaller distance.  
    	}
    }
    
    /**
     * compute absolute value 
     * @param x
     * @return
     */
    static int abs(int x) {
		if (x < 0) {
			return -x;
		} else {
			return x;
		}
	}
    /**
     * Returns the result of the closest pair via the naive brute force algorithm
     * @param pts
     * @return
     */
    public static Result naiveClosestPair(XYPoint pts[]) {
    	Result result = new Result(pts[0], pts[0], INF);
    	double temp_d;
    	int i,j;
    	for(i = 0; i<pts.length-1; i++){
    		for(j = i+1; j<pts.length;j++){
    			if((temp_d = pts[i].dist(pts[j]))<=result.dist){
    				if(temp_d == result.dist){
    					if(pts[i].isLeftOf(result.p1) || pts[i].isLeftOf(result.p2)){
    						result.p1 = pts[i];
        					result.p2 = pts[j];
        					result.dist = temp_d;
    					}
    				}else{
    					result.p1 = pts[i];
    					result.p2 = pts[j];
    					result.dist = temp_d;
    				}
    			}
    		}
    	}
    	return result;
    }
    
}

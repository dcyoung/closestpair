package ClosestPair;
import java.util.Random;
import java.util.List;

public class Sort {
	
    //================================= sort =================================
    //
    // Input: array A of XYPoints refs 
    //        lessThan is the function used to compare points
    //
    // Output: Upon completion, array A is sorted in nondecreasing order
    //         by lessThan.
    //=========================================================================
    
	/**
	 * msort
	 * @param A
	 * @param lessThan
	 */
	public static void msort(XYPoint[] A, Comparator lessThan) {
	   //Implement your sort here.  (use mergeSort).
	   if (A.length!=0 && A.length !=1){
			if(A.length<=25)
				insertion_Sort(A,lessThan, 0, A.length); 
			else
				msort_recursive_helper(A, 0, A.length-1,lessThan);
		}
    }
	
	/**
	 * insertionSort
	 * @param A
	 * @param lessThan
	 * @param start
	 * @param end
	 */
	private static void insertion_Sort(XYPoint[] A,Comparator lessThan, int start, int end) {
		
		for (int i = start + 1; i < end; i++){
			XYPoint val = A[i];
			int j = i-1;
			while (j >= 0 && lessThan.comp(val, A[j])){
				A[j + 1] = A[j];
				j--;
			}
			A[j + 1] = val;
		}
	}
	
	/**
	 * msort_recursive_helper
	 * @param A
	 * @param first_index
	 * @param last_index
	 * @param lessThan
	 */
	public static void msort_recursive_helper(XYPoint[] A, int first_index, int last_index, Comparator lessThan) {
		if (first_index < last_index) {
			int mid_pt_index = (first_index+last_index)/2; 
			
			//Recursive Step... sort each half and then merge them....
			msort_recursive_helper(A, first_index, mid_pt_index,lessThan);
			msort_recursive_helper(A, mid_pt_index+1, last_index,lessThan);
			combine(A, first_index, mid_pt_index, last_index, lessThan);
		}
	}
	
	/**
	 * combine
	 * @param A
	 * @param first_index
	 * @param last_index
	 * @param lessThan
	 */
	public static void combine(XYPoint[] A, int first_index, int mid_index, int last_index, Comparator lessThan) {
		//really ghetto cheat trick for later so that I can skirt having to look for null pointers
		int max_pt_val = 2147483647;//Integer.MAX_VALUE;
		XYPoint biggest_pt_ever = new XYPoint(max_pt_val, max_pt_val);
		
		//Determine the sizes of each of the arrays you will create for the left and right portions
		int Lsize = mid_index - first_index + 1;
		int Rsize = last_index - mid_index;
		
		//Initialize the two arrays of points that will hold the left and right points
		XYPoint[] L_pts = new XYPoint[Lsize+1];
		XYPoint[] R_pts = new XYPoint[Rsize+1];
		
		//Populate the arrays
		int smaller_val = Math.min(Lsize,Rsize);
		int i;
		for(i = 0; i<smaller_val; i++){
			L_pts[i] = A[first_index+i];
			R_pts[i] = A[mid_index+i+1];
		}
		if(Lsize>Rsize){
			for(i = smaller_val; i< Lsize; i++)
				L_pts[i] = A[first_index+i];
		}else{
			for(i = smaller_val; i< Lsize; i++)
				R_pts[i] = A[mid_index+i+1];
		}
		L_pts[Lsize] = biggest_pt_ever;
		R_pts[Rsize] = biggest_pt_ever;
		
		//do the actual merge... by comparing the points from each array and then copying them over.
		int curr_L_index,curr_R_index,curr_A_index;
		curr_L_index = 0;
		curr_R_index = 0;
		for (curr_A_index = first_index; curr_A_index <= last_index; curr_A_index++) {
			if (lessThan.comp(L_pts[curr_L_index], R_pts[curr_R_index])) {
				A[curr_A_index] = L_pts[curr_L_index];
				curr_L_index++;
			}
			else {
				A[curr_A_index] = R_pts[curr_R_index];
				curr_R_index++;
			}
		}
	}

}


